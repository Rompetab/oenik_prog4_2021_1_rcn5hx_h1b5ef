var class_chess_1_1_game_model_1_1_game_item =
[
    [ "GameItem", "class_chess_1_1_game_model_1_1_game_item.html#a430c8eac2558afba5970952f5ec4d2bb", null ],
    [ "Equals", "class_chess_1_1_game_model_1_1_game_item.html#a2f5d41e12846b6a2c69c5af96834d6b2", null ],
    [ "GetHashCode", "class_chess_1_1_game_model_1_1_game_item.html#abe7257e83de731447ecd859a98372e2b", null ],
    [ "Color", "class_chess_1_1_game_model_1_1_game_item.html#ae3b4ed2fcee7d523f109a292629a8e09", null ],
    [ "Moves", "class_chess_1_1_game_model_1_1_game_item.html#a3b24b386c242511cc9214a7c3bbd3e58", null ],
    [ "Name", "class_chess_1_1_game_model_1_1_game_item.html#a82afbec9a18ee14142d9ff18b50f2e10", null ]
];