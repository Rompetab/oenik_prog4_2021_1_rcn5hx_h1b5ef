var annotated_dup =
[
    [ "Chess", "namespace_chess.html", [
      [ "GameLogic", "namespace_chess_1_1_game_logic.html", [
        [ "MoveInterface", "namespace_chess_1_1_game_logic_1_1_move_interface.html", [
          [ "ILegalMove", "interface_chess_1_1_game_logic_1_1_move_interface_1_1_i_legal_move.html", "interface_chess_1_1_game_logic_1_1_move_interface_1_1_i_legal_move" ]
        ] ],
        [ "LegalMove", "class_chess_1_1_game_logic_1_1_legal_move.html", "class_chess_1_1_game_logic_1_1_legal_move" ],
        [ "MovePieces", "class_chess_1_1_game_logic_1_1_move_pieces.html", "class_chess_1_1_game_logic_1_1_move_pieces" ],
        [ "PawnPromotion", "class_chess_1_1_game_logic_1_1_pawn_promotion.html", "class_chess_1_1_game_logic_1_1_pawn_promotion" ],
        [ "ScoreCount", "class_chess_1_1_game_logic_1_1_score_count.html", "class_chess_1_1_game_logic_1_1_score_count" ]
      ] ],
      [ "GameModel", "namespace_chess_1_1_game_model.html", [
        [ "Pieces", "namespace_chess_1_1_game_model_1_1_pieces.html", [
          [ "Bishop", "class_chess_1_1_game_model_1_1_pieces_1_1_bishop.html", "class_chess_1_1_game_model_1_1_pieces_1_1_bishop" ],
          [ "BlackPawn", "class_chess_1_1_game_model_1_1_pieces_1_1_black_pawn.html", "class_chess_1_1_game_model_1_1_pieces_1_1_black_pawn" ],
          [ "King", "class_chess_1_1_game_model_1_1_pieces_1_1_king.html", "class_chess_1_1_game_model_1_1_pieces_1_1_king" ],
          [ "Knight", "class_chess_1_1_game_model_1_1_pieces_1_1_knight.html", "class_chess_1_1_game_model_1_1_pieces_1_1_knight" ],
          [ "Queen", "class_chess_1_1_game_model_1_1_pieces_1_1_queen.html", "class_chess_1_1_game_model_1_1_pieces_1_1_queen" ],
          [ "Rook", "class_chess_1_1_game_model_1_1_pieces_1_1_rook.html", "class_chess_1_1_game_model_1_1_pieces_1_1_rook" ],
          [ "WhitePawn", "class_chess_1_1_game_model_1_1_pieces_1_1_white_pawn.html", "class_chess_1_1_game_model_1_1_pieces_1_1_white_pawn" ]
        ] ],
        [ "Board", "class_chess_1_1_game_model_1_1_board.html", "class_chess_1_1_game_model_1_1_board" ],
        [ "GameItem", "class_chess_1_1_game_model_1_1_game_item.html", "class_chess_1_1_game_model_1_1_game_item" ],
        [ "IGameModel", "interface_chess_1_1_game_model_1_1_i_game_model.html", "interface_chess_1_1_game_model_1_1_i_game_model" ],
        [ "Model", "class_chess_1_1_game_model_1_1_model.html", "class_chess_1_1_game_model_1_1_model" ],
        [ "Tile", "class_chess_1_1_game_model_1_1_tile.html", "class_chess_1_1_game_model_1_1_tile" ]
      ] ],
      [ "GameRenderer", "namespace_chess_1_1_game_renderer.html", [
        [ "Renderer", "class_chess_1_1_game_renderer_1_1_renderer.html", "class_chess_1_1_game_renderer_1_1_renderer" ]
      ] ],
      [ "App", "class_chess_1_1_app.html", "class_chess_1_1_app" ],
      [ "ChessControl", "class_chess_1_1_chess_control.html", "class_chess_1_1_chess_control" ],
      [ "ChessGameWindow", "class_chess_1_1_chess_game_window.html", "class_chess_1_1_chess_game_window" ],
      [ "MainWindow", "class_chess_1_1_main_window.html", "class_chess_1_1_main_window" ],
      [ "PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", "class_chess_1_1_pawn_promotion_window" ]
    ] ],
    [ "ChessGameTest", "namespace_chess_game_test.html", [
      [ "MoveLogicTest", "class_chess_game_test_1_1_move_logic_test.html", "class_chess_game_test_1_1_move_logic_test" ]
    ] ],
    [ "Repository", "namespace_repository.html", [
      [ "IRepositoryStorage", "interface_repository_1_1_i_repository_storage.html", "interface_repository_1_1_i_repository_storage" ],
      [ "Repo", "class_repository_1_1_repo.html", "class_repository_1_1_repo" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];