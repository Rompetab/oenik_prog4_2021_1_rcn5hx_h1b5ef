var namespace_chess_1_1_game_logic =
[
    [ "MoveInterface", "namespace_chess_1_1_game_logic_1_1_move_interface.html", "namespace_chess_1_1_game_logic_1_1_move_interface" ],
    [ "LegalMove", "class_chess_1_1_game_logic_1_1_legal_move.html", "class_chess_1_1_game_logic_1_1_legal_move" ],
    [ "MovePieces", "class_chess_1_1_game_logic_1_1_move_pieces.html", "class_chess_1_1_game_logic_1_1_move_pieces" ],
    [ "PawnPromotion", "class_chess_1_1_game_logic_1_1_pawn_promotion.html", "class_chess_1_1_game_logic_1_1_pawn_promotion" ],
    [ "ScoreCount", "class_chess_1_1_game_logic_1_1_score_count.html", "class_chess_1_1_game_logic_1_1_score_count" ]
];