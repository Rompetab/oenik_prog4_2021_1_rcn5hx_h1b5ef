var hierarchy =
[
    [ "Application", null, [
      [ "Chess.App", "class_chess_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "Chess.App", "class_chess_1_1_app.html", null ],
      [ "Chess.App", "class_chess_1_1_app.html", null ]
    ] ],
    [ "Chess.GameModel.Board", "class_chess_1_1_game_model_1_1_board.html", null ],
    [ "FrameworkElement", null, [
      [ "Chess.ChessControl", "class_chess_1_1_chess_control.html", null ]
    ] ],
    [ "Chess.GameModel.GameItem", "class_chess_1_1_game_model_1_1_game_item.html", [
      [ "Chess.GameModel.Pieces.Bishop", "class_chess_1_1_game_model_1_1_pieces_1_1_bishop.html", null ],
      [ "Chess.GameModel.Pieces.BlackPawn", "class_chess_1_1_game_model_1_1_pieces_1_1_black_pawn.html", null ],
      [ "Chess.GameModel.Pieces.King", "class_chess_1_1_game_model_1_1_pieces_1_1_king.html", null ],
      [ "Chess.GameModel.Pieces.Knight", "class_chess_1_1_game_model_1_1_pieces_1_1_knight.html", null ],
      [ "Chess.GameModel.Pieces.Queen", "class_chess_1_1_game_model_1_1_pieces_1_1_queen.html", null ],
      [ "Chess.GameModel.Pieces.Rook", "class_chess_1_1_game_model_1_1_pieces_1_1_rook.html", null ],
      [ "Chess.GameModel.Pieces.WhitePawn", "class_chess_1_1_game_model_1_1_pieces_1_1_white_pawn.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "Chess.ChessGameWindow", "class_chess_1_1_chess_game_window.html", null ],
      [ "Chess.ChessGameWindow", "class_chess_1_1_chess_game_window.html", null ],
      [ "Chess.MainWindow", "class_chess_1_1_main_window.html", null ],
      [ "Chess.MainWindow", "class_chess_1_1_main_window.html", null ],
      [ "Chess.PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", null ],
      [ "Chess.PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", null ]
    ] ],
    [ "Chess.GameModel.IGameModel", "interface_chess_1_1_game_model_1_1_i_game_model.html", [
      [ "Chess.GameModel.Model", "class_chess_1_1_game_model_1_1_model.html", null ]
    ] ],
    [ "Chess.GameLogic.MoveInterface.ILegalMove", "interface_chess_1_1_game_logic_1_1_move_interface_1_1_i_legal_move.html", [
      [ "Chess.GameLogic.LegalMove", "class_chess_1_1_game_logic_1_1_legal_move.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repository.IRepositoryStorage", "interface_repository_1_1_i_repository_storage.html", null ],
    [ "ChessGameTest.MoveLogicTest", "class_chess_game_test_1_1_move_logic_test.html", null ],
    [ "Chess.GameLogic.MovePieces", "class_chess_1_1_game_logic_1_1_move_pieces.html", null ],
    [ "Chess.GameLogic.PawnPromotion", "class_chess_1_1_game_logic_1_1_pawn_promotion.html", null ],
    [ "Chess.GameRenderer.Renderer", "class_chess_1_1_game_renderer_1_1_renderer.html", null ],
    [ "Repository.Repo", "class_repository_1_1_repo.html", null ],
    [ "Chess.GameLogic.ScoreCount", "class_chess_1_1_game_logic_1_1_score_count.html", null ],
    [ "Chess.GameModel.Tile", "class_chess_1_1_game_model_1_1_tile.html", null ],
    [ "System.Windows.Window", null, [
      [ "Chess.ChessGameWindow", "class_chess_1_1_chess_game_window.html", null ],
      [ "Chess.ChessGameWindow", "class_chess_1_1_chess_game_window.html", null ],
      [ "Chess.MainWindow", "class_chess_1_1_main_window.html", null ],
      [ "Chess.MainWindow", "class_chess_1_1_main_window.html", null ],
      [ "Chess.MainWindow", "class_chess_1_1_main_window.html", null ],
      [ "Chess.PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", null ],
      [ "Chess.PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", null ],
      [ "Chess.PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Chess.ChessGameWindow", "class_chess_1_1_chess_game_window.html", null ]
    ] ]
];