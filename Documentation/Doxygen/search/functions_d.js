var searchData=
[
  ['savegame_158',['SaveGame',['../interface_repository_1_1_i_repository_storage.html#af38095e9b7fe567ea9aa06574cdecba7',1,'Repository.IRepositoryStorage.SaveGame()'],['../class_repository_1_1_repo.html#abfddd757c4ae9d13d9816fe26ae02b24',1,'Repository.Repo.SaveGame()']]],
  ['scorecount_159',['ScoreCount',['../class_chess_1_1_game_logic_1_1_score_count.html#a60c111d400bff8e70d2116a6fd32c68f',1,'Chess::GameLogic::ScoreCount']]],
  ['scoretest_160',['ScoreTest',['../class_chess_game_test_1_1_move_logic_test.html#a754d12473803d87a00bcccc174eb4ce8',1,'ChessGameTest::MoveLogicTest']]],
  ['setpropertyvalue_161',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['somerenderwhencheck_162',['SomeRenderWhenCheck',['../class_chess_1_1_game_logic_1_1_legal_move.html#a577eef25b0b31f47fb315b88cbec4404',1,'Chess.GameLogic.LegalMove.SomeRenderWhenCheck()'],['../interface_chess_1_1_game_logic_1_1_move_interface_1_1_i_legal_move.html#a3ef47e00c1431d8190e5c48558d63b47',1,'Chess.GameLogic.MoveInterface.ILegalMove.SomeRenderWhenCheck()']]]
];
