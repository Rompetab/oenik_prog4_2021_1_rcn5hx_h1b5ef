var class_chess_1_1_game_model_1_1_model =
[
    [ "Model", "class_chess_1_1_game_model_1_1_model.html#a470c349db0d96c682139695b552ef052", null ],
    [ "Board", "class_chess_1_1_game_model_1_1_model.html#a6c44d38b358b9b6b3e11b5ca853f6c38", null ],
    [ "GameHeight", "class_chess_1_1_game_model_1_1_model.html#a2d5ec4a77b9c8eb45bf0479e47f43a9b", null ],
    [ "GameWidth", "class_chess_1_1_game_model_1_1_model.html#a0eb9d72e55219ee2998e33e2d62c33a4", null ],
    [ "TileSize", "class_chess_1_1_game_model_1_1_model.html#a727a61d521c45d236e1ee4fbe84a6c64", null ]
];