var namespace_chess =
[
    [ "GameLogic", "namespace_chess_1_1_game_logic.html", "namespace_chess_1_1_game_logic" ],
    [ "GameModel", "namespace_chess_1_1_game_model.html", "namespace_chess_1_1_game_model" ],
    [ "GameRenderer", "namespace_chess_1_1_game_renderer.html", "namespace_chess_1_1_game_renderer" ],
    [ "App", "class_chess_1_1_app.html", "class_chess_1_1_app" ],
    [ "ChessControl", "class_chess_1_1_chess_control.html", "class_chess_1_1_chess_control" ],
    [ "ChessGameWindow", "class_chess_1_1_chess_game_window.html", "class_chess_1_1_chess_game_window" ],
    [ "MainWindow", "class_chess_1_1_main_window.html", "class_chess_1_1_main_window" ],
    [ "PawnPromotionWindow", "class_chess_1_1_pawn_promotion_window.html", "class_chess_1_1_pawn_promotion_window" ]
];