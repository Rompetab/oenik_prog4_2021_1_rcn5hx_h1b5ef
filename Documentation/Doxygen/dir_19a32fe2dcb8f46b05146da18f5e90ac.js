var dir_19a32fe2dcb8f46b05146da18f5e90ac =
[
    [ "Chess", "dir_b487e74e94199721f239b00acb7518e1.html", "dir_b487e74e94199721f239b00acb7518e1" ],
    [ "Chess.GameLogic", "dir_30b0e8cfebd4312234a5e28bff451349.html", "dir_30b0e8cfebd4312234a5e28bff451349" ],
    [ "Chess.GameModel", "dir_84a4c9f0b079902d942f823d1cd93020.html", "dir_84a4c9f0b079902d942f823d1cd93020" ],
    [ "Chess.GameRenderer", "dir_00ff34694023c05d05be7c8b09577168.html", "dir_00ff34694023c05d05be7c8b09577168" ],
    [ "ChessGameTest", "dir_578b566edc1e948a92b4eab58440787c.html", "dir_578b566edc1e948a92b4eab58440787c" ],
    [ "Repository", "dir_1649f260aa58f182794193b9a2b7a48b.html", "dir_1649f260aa58f182794193b9a2b7a48b" ]
];