﻿// <copyright file="ChessControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Chess.GameLogic;
    using Chess.GameModel;
    using Chess.GameModel.Pieces;
    using Chess.GameRenderer;
    using Repository;

    /// <summary>
    /// Controls the whole game.
    /// </summary>
    public class ChessControl : FrameworkElement
    {
        /// <summary>
        /// Selected piece thru Pawn Promotion window.
        /// </summary>
        internal static GameItem SelectedPiece;

        /// <summary>
        /// Sets a value if the player want a new game.
        /// </summary>
        internal static bool NewOrLoad;
        private LegalMove legalMove;
        private Renderer renderer;
        private Model model;
        private MovePieces movePiece;
        private PawnPromotion pawnPromotion;
        private ScoreCount scoreCount;
        private Repo repository;
        private int index;
        private int step;
        private Point tileCurrentPos;
        private Point tileFinishPos;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChessControl"/> class.
        /// </summary>
        public ChessControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Renders a frame.
        /// </summary>
        /// <param name="drawingContext">Gets a drawingContext.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                drawingContext?.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new Model(this.ActualHeight, this.ActualWidth);

            this.movePiece = new MovePieces(this.model);
            this.legalMove = new LegalMove(this.model);
            this.pawnPromotion = new PawnPromotion(this.model);
            this.repository = new Repo(this.model);
            this.scoreCount = new ScoreCount(this.model);
            this.renderer = new Renderer(this.model);

            this.model.TileSize = Math.Min(this.model.GameWidth / this.ActualHeight * 62.5, this.model.GameHeight / this.ActualHeight * 62.5);
            if (NewOrLoad)
            {
                this.step = this.repository.LoadGame();
                int black = 0;
                int white = 0;
                int blackAll = 39;
                int whiteAll = 39;
                for (int x = 0; x < this.model.Board.Size; x++)
                {
                    for (int y = 0; y < this.model.Board.Size; y++)
                    {
                        if (this.model.Board.Tiles[x, y].Piece != null && this.model.Board.Tiles[x, y].Piece.Color)
                        {
                            black += this.scoreCount.Count(this.model.Board.Tiles[x, y].Piece);
                        }
                        else if (this.model.Board.Tiles[x, y].Piece != null)
                        {
                            white += this.scoreCount.Count(this.model.Board.Tiles[x, y].Piece);
                        }
                    }
                }

                this.renderer.BlackPoints = blackAll - black;
                this.renderer.WhitePoints = whiteAll - white;
            }
            else
            {
                this.model.Board = new Board(8);
                this.model.Board.CreateNew();
            }

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.MouseDown += this.GameControl_MouseDown;
            }

            this.InvalidateVisual();
        }

        private void GameControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Point mousePos = e.GetPosition(this);
            if (this.index == 0)
            {
                this.tileCurrentPos = this.movePiece.GetTilePos(mousePos);
                if (this.tileCurrentPos.X <= 7 && this.tileCurrentPos.Y <= 7 && this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece != null)
                {
                    if ((this.step % 2 == 0 && !this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Color) || (this.step % 2 == 1 && this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Color))
                    {
                        this.renderer.LegalTilesArray = this.legalMove.SomeRenderWhenCheck(this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y]);
                        this.index++;
                    }
                }
            }
            else
            {
                this.tileFinishPos = this.movePiece.GetTilePos(mousePos);
                this.index = 0;
                if (this.tileFinishPos.X <= 7 && this.tileFinishPos.Y <= 7 && this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece != null)
                {
                    if (((this.step % 2 == 0 && !this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Color) || (this.step % 2 == 1 && this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Color)) && this.renderer.LegalTilesArray[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y])
                    {
                        GameItem piece = this.model.Board.Tiles[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y].Piece;
                        if (this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece != null && this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Name == "P" && ((int)this.tileFinishPos.Y == 0 || (int)this.tileFinishPos.Y == 7))
                        {
                            bool pieceColor = this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece.Color;
                            List<GameItem> pieces = new List<GameItem> { new Queen(pieceColor, "Q"), new Rook(pieceColor, "R"), new Bishop(pieceColor, "B"), new Knight(pieceColor, "N") };
                            PawnPromotionWindow window = new PawnPromotionWindow(pieces, SelectedPiece);
                            bool? close = window.ShowDialog();
                            if (close == true)
                            {
                                PawnPromotion.Promotion(this.model.Board.Tiles[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y], SelectedPiece);
                                this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y].Piece = null;
                                if (this.tileCurrentPos != this.tileFinishPos)
                                {
                                    this.step++;

                                    if (piece != null)
                                    {
                                        if (this.step % 2 == 0)
                                        {
                                            this.renderer.WhitePoints += this.scoreCount.Count(piece);
                                        }
                                        else
                                        {
                                            this.renderer.BlackPoints += this.scoreCount.Count(piece);
                                        }
                                    }

                                    this.repository.SaveGame(this.step);
                                }
                            }
                        }
                        else
                        {
                            MovePieces.Move(this.model.Board.Tiles[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y], this.model.Board.Tiles[(int)this.tileCurrentPos.X, (int)this.tileCurrentPos.Y]);
                            if (!this.legalMove.Matt(!this.model.Board.Tiles[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y].Piece.Color))
                            {
                                if (this.tileCurrentPos != this.tileFinishPos)
                                {
                                    this.step++;

                                    if (piece != null)
                                    {
                                        if (this.step % 2 == 0)
                                        {
                                            this.renderer.WhitePoints += this.scoreCount.Count(piece);
                                        }
                                        else
                                        {
                                            this.renderer.BlackPoints += this.scoreCount.Count(piece);
                                        }
                                    }

                                    this.repository.SaveGame(this.step);
                                }
                            }
                            else
                            {
                                this.renderer.LegalTilesArray = null;
                                this.InvalidateVisual();
                                MessageBox.Show("A nyertes pedig a " + (this.model.Board.Tiles[(int)this.tileFinishPos.X, (int)this.tileFinishPos.Y].Piece.Color == true ? "fekete" : "fehér") + "!\nElég bénafej az ellenfél nyenyenye!");
                            }
                        }
                    }
                }

                this.renderer.LegalTilesArray = null;
            }

            this.InvalidateVisual();
        }
    }
}
