﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "We use List<GameItem> not generic types.", Scope = "member", Target = "~M:Chess.PawnPromotionWindow.#ctor(System.Collections.Generic.List{Chess.GameModel.GameItem},Chess.GameModel.GameItem)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Can not be private because of the usage.", Scope = "member", Target = "~F:Chess.ChessControl.SelectedPiece")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Can not be private because of the usage.", Scope = "member", Target = "~F:Chess.ChessControl.NewOrLoad")]
