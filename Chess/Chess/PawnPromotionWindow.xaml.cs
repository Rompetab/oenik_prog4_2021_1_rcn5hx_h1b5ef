﻿// <copyright file="PawnPromotionWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Chess.GameModel;

    /// <summary>
    /// Interaction logic for PawnPromotionWindow.xaml.
    /// </summary>
    public partial class PawnPromotionWindow : Window
    {
        private List<GameItem> pieces;
        private GameItem selectedPiece;

        /// <summary>
        /// Initializes a new instance of the <see cref="PawnPromotionWindow"/> class.
        /// </summary>
        public PawnPromotionWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PawnPromotionWindow"/> class.
        /// </summary>
        /// <param name="pieces">pieces we need to display.</param>
        /// <param name="selectedPiece">the selectedPiece we selected.</param>
        public PawnPromotionWindow(List<GameItem> pieces, GameItem selectedPiece)
            : this()
        {
            this.PiecesList.ItemsSource = pieces;
            this.pieces = pieces;
            this.selectedPiece = selectedPiece;
        }

        private void PiecesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChessControl.SelectedPiece = this.pieces[this.PiecesList.SelectedIndex];
            this.DialogResult = true;
        }
    }
}
