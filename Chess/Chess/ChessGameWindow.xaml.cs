﻿// <copyright file="ChessGameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ChessGameWindow.xaml.
    /// </summary>
    public partial class ChessGameWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChessGameWindow"/> class.
        /// </summary>
        public ChessGameWindow()
        {
            this.InitializeComponent();
        }
    }
}
