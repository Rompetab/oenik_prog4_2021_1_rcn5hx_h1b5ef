﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "It is an 8 by 8 chess board. It would be more comlex and less meaningful to make covert to jaggedArray.", Scope = "member", Target = "~P:Chess.GameRenderer.Renderer.LegalTilesArray")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "It needs to Gets or Sets the legal moves.", Scope = "member", Target = "~P:Chess.GameRenderer.Renderer.LegalTilesArray")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "JaggedArray warning again. We prefere matrices.", Scope = "member", Target = "~M:Chess.GameRenderer.Renderer.GetBoardPieces~System.Windows.Media.Drawing")]
