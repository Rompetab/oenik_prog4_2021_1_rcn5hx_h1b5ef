﻿// <copyright file="Renderer.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Text;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Chess.GameLogic;
    using Chess.GameModel;

    /// <summary>
    /// Renders the game.
    /// </summary>
    public class Renderer
    {
        private Model model;
        private Drawing whiteTiles;
        private Drawing blackTiles;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();
        private Board oldBoard;

        private Typeface font = new Typeface("Arial");
        private FormattedText formattedTextWhite;
        private FormattedText formattedTextBlack;

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="model">A model instance.</param>
        public Renderer(Model model)
        {
            this.model = model;
        }

        /// <summary>
        /// Gets or Sets the legal moves, for the given piece.
        /// </summary>
        public bool[,] LegalTilesArray { get; set; }

        /// <summary>
        /// Gets or Sets how many points the white has.
        /// </summary>
        public int WhitePoints { get; set; }

        /// <summary>
        /// Gets or Sets how many points the black has.
        /// </summary>
        public int BlackPoints { get; set; }

        private Brush BlackBishop
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackBishop.bmp");
            }
        }

        private Brush BlackKnight
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackKnight.bmp");
            }
        }

        private Brush BlackKing
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackKing.bmp");
            }
        }

        private Brush BlackPawn
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackPawn.bmp");
            }
        }

        private Brush BlackQueen
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackQueen.bmp");
            }
        }

        private Brush BlackRook
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.BlackRook.bmp");
            }
        }

        private Brush WhiteBishop
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhiteBishop.bmp");
            }
        }

        private Brush WhiteKnight
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhiteKnight.bmp");
            }
        }

        private Brush WhiteKing
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhiteKing.bmp");
            }
        }

        private Brush WhitePawn
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhitePawn.bmp");
            }
        }

        private Brush WhiteQueen
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhiteQueen.bmp");
            }
        }

        private Brush WhiteRook
        {
            get
            {
                return this.GetBrush("Chess.GameRenderer.ChessPieceImages.WhiteRook.bmp");
            }
        }

        /// <summary>
        /// Connects the brushnames with the correct file.
        /// </summary>
        /// <param name="fname">the name of the file.</param>
        /// <returns>an ImageBrush by its name.</returns>
        public Brush GetBrush(string fname)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        /// <summary>
        /// Builds all the render elements.
        /// </summary>
        /// <returns>a drawing, which contains all the rendering elements.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBlackTiles());
            dg.Children.Add(this.GetWhiteTiles());
            dg.Children.Add(this.GetLegalTiles());
            dg.Children.Add(this.GetBoardPieces());
            dg.Children.Add(this.GetScore());

            return dg;
        }

        private Drawing GetLegalTiles()
        {
            DrawingGroup w = new DrawingGroup();
            for (int x = 0; x < this.model.Board.Size; x++)
            {
                for (int y = 0; y < this.model.Board.Size; y++)
                {
                    if (this.LegalTilesArray != null && this.LegalTilesArray[x, y])
                    {
                        Geometry bigCircle = new EllipseGeometry(new Rect((x * this.model.TileSize) + 2.5, (y * this.model.TileSize) + 2.5, this.model.TileSize - 5, this.model.TileSize - 5));
                        Geometry smallCircle = new EllipseGeometry(new Rect((x * this.model.TileSize) + 20, (y * this.model.TileSize) + 20, this.model.TileSize - 40, this.model.TileSize - 40));
                        if (this.model.Board.Tiles[x, y].Piece != null)
                        {
                            w.Children.Add(new GeometryDrawing(null, new Pen(Brushes.Gray, 5), bigCircle));
                        }
                        else
                        {
                            w.Children.Add(new GeometryDrawing(Brushes.Gray, new Pen(Brushes.Black, 1), smallCircle));
                        }
                    }
                }
            }

            return w;
        }

        private Drawing GetWhiteTiles()
        {
            GeometryGroup w = new GeometryGroup();

            for (int x = 0; x < this.model.Board.Size; x++)
            {
                for (int y = 0; y < this.model.Board.Size; y++)
                {
                    if (this.model.Board.Tiles[x, y].Dx % 2 == 1 && this.model.Board.Tiles[x, y].Dy % 2 == 1)
                    {
                        Geometry box = new RectangleGeometry(new Rect(x * this.model.TileSize, y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        w.Children.Add(box);
                    }
                    else if (this.model.Board.Tiles[x, y].Dx % 2 == 0 && this.model.Board.Tiles[x, y].Dy % 2 == 0)
                    {
                        Geometry box = new RectangleGeometry(new Rect(x * this.model.TileSize, y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        w.Children.Add(box);
                    }
                }
            }

            this.whiteTiles = new GeometryDrawing((Brush)new BrushConverter().ConvertFrom("#EEEED2"), null, w);
            return this.whiteTiles;
        }

        private Drawing GetBlackTiles()
        {
            GeometryGroup b = new GeometryGroup();
            for (int x = 0; x < this.model.Board.Size; x++)
            {
                for (int y = 0; y < this.model.Board.Size; y++)
                {
                    if (this.model.Board.Tiles[x, y].Dx % 2 == 1 && this.model.Board.Tiles[x, y].Dy % 2 == 0)
                    {
                        Geometry box = new RectangleGeometry(new Rect(x * this.model.TileSize, y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        b.Children.Add(box);
                    }
                    else if (this.model.Board.Tiles[x, y].Dx % 2 == 0 && this.model.Board.Tiles[x, y].Dy % 2 == 1)
                    {
                        Geometry box = new RectangleGeometry(new Rect(x * this.model.TileSize, y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                        b.Children.Add(box);
                    }
                }
            }

            this.blackTiles = new GeometryDrawing((Brush)new BrushConverter().ConvertFrom("#769656"), null, b);
            return this.blackTiles;
        }

        private Drawing GetScore()
        {
            DrawingGroup drawingGroup = new DrawingGroup();
            using (DrawingContext drawingContext = drawingGroup.Open())
            {
                this.formattedTextWhite = new FormattedText(
                    this.WhitePoints.ToString(CultureInfo.CurrentCulture),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    this.font,
                    30,
                    Brushes.Black,
                    1.0);

                this.formattedTextBlack =
                    new FormattedText(
                    this.BlackPoints.ToString(CultureInfo.CurrentCulture),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    this.font,
                    30,
                    Brushes.Black,
                    1.0);

                Geometry textGeometryWhite = this.formattedTextWhite.BuildGeometry(new Point(510, 0));
                Geometry textGeometryBlack = this.formattedTextBlack.BuildGeometry(new Point(510, 450));
                drawingContext.DrawGeometry(Brushes.Gold, new Pen(Brushes.Maroon, 1.5), textGeometryWhite);
                drawingContext.DrawGeometry(Brushes.Gold, new Pen(Brushes.Maroon, 1.5), textGeometryBlack);
                return drawingGroup;
            }
        }

        private Drawing GetBoardPieces()
        {
            DrawingGroup dg = new DrawingGroup();
            Brush[,] brushes = new Brush[,]
            {
                { this.BlackPawn, this.WhitePawn },
                { this.BlackKnight, this.WhiteKnight },
                { this.BlackKing, this.WhiteKing },
                { this.BlackQueen, this.WhiteQueen },
                { this.BlackRook, this.WhiteRook },
                { this.BlackBishop, this.WhiteBishop },
            };

            string[] names = new string[] { "P", "N", "K", "Q", "R", "B" };

            if (this.oldBoard == null || this.oldBoard.Equals(this.model.Board))
            {
                for (int i = 0; i < this.model.Board.Size; i++)
                {
                    for (int j = 0; j < this.model.Board.Size; j++)
                    {
                        for (int k = 0; k < names.Length; k++)
                        {
                            Geometry geo = new RectangleGeometry(new Rect(this.model.Board.Tiles[i, j].Dx * this.model.TileSize, this.model.Board.Tiles[i, j].Dy * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            if (this.model.Board.Tiles[i, j].Piece != null)
                            {
                                if (this.model.Board.Tiles[i, j].Piece.Name == names[k])
                                {
                                    if (!this.model.Board.Tiles[i, j].Piece.Color)
                                    {
                                        dg.Children.Add(new GeometryDrawing(brushes[k, 1], null, geo));
                                    }
                                    else if (this.model.Board.Tiles[i, j].Piece.Color)
                                    {
                                        dg.Children.Add(new GeometryDrawing(brushes[k, 0], null, geo));
                                    }
                                }
                            }
                        }

                        this.oldBoard = this.model.Board;
                    }
                }
            }

            return dg;
        }
    }
}
