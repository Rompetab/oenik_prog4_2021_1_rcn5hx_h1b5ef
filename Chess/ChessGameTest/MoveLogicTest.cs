// <copyright file="MoveLogicTest.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace ChessGameTest
{
    using Chess.GameLogic;
    using Chess.GameModel;
    using Chess.GameModel.Pieces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests the game.
    /// </summary>
    public class MoveLogicTest
    {
        /// <summary>
        /// Tests of if the move method is working right.
        /// </summary>
        [Test]
        public void MoveTest()
        {
            Mock<IGameModel> model = new Mock<IGameModel>();
            MovePieces movePiece = new MovePieces(model.Object);
            Tile t1 = new Tile(3, 1);
            Tile t2 = new Tile(3, 2);

            GameItem item = new Rook(true, "R");
            t2.Piece = item;

            MovePieces.Move(t1, t2);

            Assert.That(t1.Piece, Is.EqualTo(item));
        }

        /// <summary>
        /// Checks if the scoretest method gives the correct value given by the name of the piece.
        /// </summary>
        /// <param name="c">the name of the piece.</param>
        /// <param name="num">the value of the piece.</param>
        [TestCase("R", 5)]
        [TestCase("Q", 9)]
        [TestCase("P", 1)]
        public void ScoreTest(string c, int num)
        {
            Mock<IGameModel> model = new Mock<IGameModel>();
            ScoreCount scoreCount = new ScoreCount(model.Object);

            GameItem item = new Rook(true, c);

            int score = scoreCount.Count(item);

            Assert.That(score, Is.EqualTo(num));
        }

        /// <summary>
        /// Checks if the legalMove method gives back a correct bool[,] array, given by a piece.
        /// </summary>
        [Test]
        public void LegalMoveTest()
        {
            Mock<IGameModel> model = new Mock<IGameModel>();
            LegalMove legalMove = new LegalMove(model.Object);
            model.Setup(x => x.Board).Returns(new Board(8));
            model.Object.Board.CreateNew();

            bool[,] legalMoves = legalMove.IsLegal(model.Object.Board.Tiles[2, 1]);

            bool[,] blackPawnLegalMoves = new bool[8, 8];
            blackPawnLegalMoves[2, 2] = true;
            blackPawnLegalMoves[2, 3] = true;

            Assert.That(legalMoves, Is.EqualTo(blackPawnLegalMoves));
        }

        /// <summary>
        /// Checks if the whereIsTheKing method gives back the correct value.
        /// </summary>
        [Test]
        public void WhereIsTheKingTest()
        {
            Mock<IGameModel> model = new Mock<IGameModel>();
            LegalMove legalMove = new LegalMove(model.Object);
            model.Setup(x => x.Board).Returns(new Board(8));
            model.Object.Board.CreateNew();

            // White king spawn position
            var resultX = legalMove.WhereIsTheKing(false).Dx;
            var resultY = legalMove.WhereIsTheKing(false).Dy;

            // Spawn Coordinates are [4,7]
            Assert.That(resultX, Is.EqualTo(4));
            Assert.That(resultY, Is.EqualTo(7));
        }

        /// <summary>
        /// Checks if the isCheck method gives back true, if the king is in check.
        /// </summary>
        [Test]
        public void IsCheckTest()
        {
            Mock<IGameModel> model = new Mock<IGameModel>();
            LegalMove legalMove = new LegalMove(model.Object);
            model.Setup(x => x.Board).Returns(new Board(8));
            model.Object.Board.CreateNew();
            MovePieces movePiece = new MovePieces(model.Object);

            // Setup
            // Move white Queen to [4,1]
            // in front of the black King
            MovePieces.Move(model.Object.Board.Tiles[4, 1], model.Object.Board.Tiles[3, 7]);

            // Is the black Queen in check now or not
            var result = legalMove.IsCheck(true);

            Assert.That(result, Is.EqualTo(true));
        }
    }
}