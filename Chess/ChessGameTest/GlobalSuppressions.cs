﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "JaggedArray  is more complexed in 8 by 8 bool matrix, no space is wasted with jaggedArray, but it's less transparent in the code.", Scope = "member", Target = "~M:ChessGameTest.MoveLogicTest.LegalMoveTest")]
