﻿// <copyright file="Repo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Chess.GameModel;
    using Chess.GameModel.Pieces;

    /// <summary>
    /// Saves and loads a board.
    /// </summary>
    public class Repo
    {
        private Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repo"/> class.
        /// </summary>
        /// <param name="model">a model instance.</param>
        public Repo(Model model)
        {
            this.model = model;
        }

        /// <summary>
        /// Loads a game from a file.
        /// </summary>
        /// <returns>a step number.</returns>
        public int LoadGame()
        {
            string load = File.ReadAllText("saveFile.txt");
            this.model.Board = new Board(8);
            string[] pieces = load.Split("|");
            for (int i = 0; i < pieces.Length - 1; i++)
            {
                int x = int.Parse(pieces[i][0].ToString(), CultureInfo.CurrentCulture);
                int y = int.Parse(pieces[i][1].ToString(), CultureInfo.CurrentCulture);
                char piece = pieces[i].ToCharArray()[2];
                if (char.IsLower(piece))
                {
                    switch (piece)
                    {
                        default:
                        case 'p':
                            this.model.Board.Tiles[x, y].Piece = new WhitePawn(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                        case 'r':
                            this.model.Board.Tiles[x, y].Piece = new Rook(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                        case 'q':
                            this.model.Board.Tiles[x, y].Piece = new Queen(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                        case 'k':
                            this.model.Board.Tiles[x, y].Piece = new King(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                        case 'n':
                            this.model.Board.Tiles[x, y].Piece = new Knight(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                        case 'b':
                            this.model.Board.Tiles[x, y].Piece = new Bishop(false, piece.ToString().ToUpper(CultureInfo.CurrentCulture));
                            break;
                    }
                }
                else
                {
                    switch (piece)
                    {
                        default:
                        case 'P':
                            this.model.Board.Tiles[x, y].Piece = new BlackPawn(true, piece.ToString());
                            break;
                        case 'R':
                            this.model.Board.Tiles[x, y].Piece = new Rook(true, piece.ToString());
                            break;
                        case 'Q':
                            this.model.Board.Tiles[x, y].Piece = new Queen(true, piece.ToString());
                            break;
                        case 'K':
                            this.model.Board.Tiles[x, y].Piece = new King(true, piece.ToString());
                            break;
                        case 'N':
                            this.model.Board.Tiles[x, y].Piece = new Knight(true, piece.ToString());
                            break;
                        case 'B':
                            this.model.Board.Tiles[x, y].Piece = new Bishop(true, piece.ToString());
                            break;
                    }
                }
            }

            return pieces[pieces.Length - 1][0];
        }

        /// <summary>
        /// Saves a game to a file.
        /// </summary>
        /// <param name="step">The step which the game states.</param>
        public void SaveGame(int step)
        {
            string save = string.Empty;

            for (int x = 0; x < this.model.Board.Size; x++)
            {
                for (int y = 0; y < this.model.Board.Size; y++)
                {
                    if (this.model.Board.Tiles[x, y].Piece != null)
                    {
                        save += x;
                        save += y;
                        save += this.model.Board.Tiles[x, y].Piece.Color == true ? this.model.Board.Tiles[x, y].Piece.Name : this.model.Board.Tiles[x, y].Piece.Name.ToLower(CultureInfo.CurrentCulture);
                        save += "|";
                    }
                }
            }

            save += step;
            File.WriteAllText("saveFile.txt", save);
        }
    }
}
