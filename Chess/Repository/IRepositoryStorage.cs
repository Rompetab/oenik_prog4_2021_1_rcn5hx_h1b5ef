﻿// <copyright file="IRepositoryStorage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// An interface for the Repository.
    /// </summary>
    public interface IRepositoryStorage
    {
        /// <summary>
        /// Saves a game to a file.
        /// </summary>
        /// <param name="everyStep">The step which the game states.</param>
        public void SaveGame(int everyStep);

        /// <summary>
        /// Loads a game from a file.
        /// </summary>
        /// <returns>a step number.</returns>
        public int LoadGame();
    }
}
