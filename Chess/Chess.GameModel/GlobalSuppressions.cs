﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Readonly is invalid in that term.", Scope = "member", Target = "~P:Chess.GameModel.GameItem.Moves")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "We prefered to use int[] not generic type collection<T>. It is cleaner and understandable in the code and to use.", Scope = "member", Target = "~P:Chess.GameModel.GameItem.Moves")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged Array warning again. ", Scope = "member", Target = "~M:Chess.GameModel.Board.#ctor(System.Int32)")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Gets or Sets all the tiles, which contains the pieces.", Scope = "member", Target = "~P:Chess.GameModel.Board.Tiles")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged Array warning again.", Scope = "member", Target = "~P:Chess.GameModel.Board.Tiles")]
