﻿// <copyright file="IGameModel.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Set the parameters for the Model class.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or Sets the width of the game.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or Sets the height of the game.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or Sets the tile size of each tile in the game.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or Sets the board of the game.
        /// </summary>
        public Board Board { get; set; }
    }
}
