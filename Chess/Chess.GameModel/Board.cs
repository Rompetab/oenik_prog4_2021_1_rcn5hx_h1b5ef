﻿// <copyright file="Board.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Chess.GameModel.Pieces;

    /// <summary>
    /// The board class, which contains all the tiles of the board and a method to create a new board.
    /// </summary>
    public class Board
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/> class.
        /// </summary>
        /// <param name="size">size of the board.</param>
        public Board(int size)
        {
            this.Size = size;
            this.Tiles = new Tile[this.Size, this.Size];
            for (int x = 0; x < this.Size; x++)
            {
                for (int y = 0; y < this.Size; y++)
                {
                    this.Tiles[x, y] = new Tile(x, y);
                }
            }
        }

        /// <summary>
        /// Gets or Sets the size of the board.
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Gets or Sets all the tiles, which contains the pieces.
        /// </summary>
        public Tile[,] Tiles { get; set; }

        /// <summary>
        /// Creates a new game.
        /// </summary>
        public void CreateNew()
        {
            // True --- fekete

            // False --- feher
            for (int i = 0; i < this.Size; i++)
            {
                this.Tiles[i, 6].Piece = new WhitePawn(false, "P");
            }

            this.Tiles[7, 7].Piece = new Rook(false, "R");
            this.Tiles[6, 7].Piece = new Knight(false, "N");
            this.Tiles[5, 7].Piece = new Bishop(false, "B");
            this.Tiles[3, 7].Piece = new Queen(false, "Q");
            this.Tiles[4, 7].Piece = new King(false, "K");
            this.Tiles[2, 7].Piece = new Bishop(false, "B");
            this.Tiles[1, 7].Piece = new Knight(false, "N");
            this.Tiles[0, 7].Piece = new Rook(false, "R");

            for (int i = 0; i < this.Size; i++)
            {
                this.Tiles[i, 1].Piece = new BlackPawn(true, "P");
            }

            this.Tiles[0, 0].Piece = new Rook(true, "R");
            this.Tiles[1, 0].Piece = new Knight(true, "N");
            this.Tiles[2, 0].Piece = new Bishop(true, "B");
            this.Tiles[4, 0].Piece = new King(true, "K");
            this.Tiles[3, 0].Piece = new Queen(true, "Q");
            this.Tiles[5, 0].Piece = new Bishop(true, "B");
            this.Tiles[6, 0].Piece = new Knight(true, "N");
            this.Tiles[7, 0].Piece = new Rook(true, "R");
        }
    }
}
