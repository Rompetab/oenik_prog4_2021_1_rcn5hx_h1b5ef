﻿// <copyright file="GameItem.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A chess piece class.
    /// </summary>
    public class GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">the name of the piece.</param>
        public GameItem(bool color, string name)
        {
            this.Color = color;
            this.Name = name;
        }

        /// <summary>
        /// Gets or sets a value indicating whether true - black or false - white.
        /// </summary>
        public bool Color { get; set; }

        /// <summary>
        /// Gets or sets the name of the piece.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets a list containing all the moves it can make from his position.
        /// </summary>
        public List<int[]> Moves { get; set; }

        /// <summary>
        /// Used for UnitTests.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>a bool, whether it is equals with the given object.</returns>
        public override bool Equals(object obj)
        {
            return this.GetType() == obj?.GetType();
        }

        /// <summary>
        /// Used for UnitTests.
        /// </summary>
        /// <returns>a unique code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
