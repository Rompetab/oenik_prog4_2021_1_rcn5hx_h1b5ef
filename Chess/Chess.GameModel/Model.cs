﻿// <copyright file="Model.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// All of the needed information, to play the game.
    /// </summary>
    public class Model : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        /// <param name="w">width of the game.</param>
        /// <param name="h">height of the game.</param>
        public Model(double w, double h)
        {
            this.GameWidth = w;
            this.GameHeight = h;
        }

        /// <summary>
        /// Gets or Sets the width of the game.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or Sets the height of the game.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or Sets the tile size of each tile in the game.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or Sets the board of the game.
        /// </summary>
        public Board Board { get; set; }
    }
}
