﻿// <copyright file="King.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel.Pieces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A King piece.
    /// </summary>
    public class King : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="King"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">name of the king.</param>
        public King(bool color, string name)
            : base(color, name)
        {
            this.Moves = new List<int[]>()
            {
                new int[] { -1, -1 },
                new int[] { -1, 0 },
                new int[] { -1, 1 },

                new int[] { 1, 1 },
                new int[] { 1, 0 },
                new int[] { 1, -1 },

                new int[] { 0, 1 },
                new int[] { 0, -1 },
            };
        }
    }
}
