﻿// <copyright file="Knight.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel.Pieces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A knight piece.
    /// </summary>
    public class Knight : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Knight"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">name of the piece.</param>
        public Knight(bool color, string name)
            : base(color, name)
        {
            this.Moves = new List<int[]>()
            {
                new int[] { -2, 1 },
                new int[] { -2, -1 },
                new int[] { 2,  1 },
                new int[] { 2,  -1 },
                new int[] { -1, -2 },
                new int[] { 1, -2 },
                new int[] { -1, 2 },
                new int[] { 1,  2 },
            };
        }
    }
}
