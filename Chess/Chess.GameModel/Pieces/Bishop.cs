﻿// <copyright file="Bishop.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel.Pieces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A bishop piece.
    /// </summary>
    public class Bishop : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bishop"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">name of the bishop.</param>
        public Bishop(bool color, string name)
            : base(color, name)
        {
            this.Moves = new List<int[]>()
        {
            // Jobb fent átló
            new int[] { 1, -1 },
            new int[] { 2, -2 },
            new int[] { 3, -3 },
            new int[] { 4, -4 },
            new int[] { 5, -5 },
            new int[] { 6, -6 },
            new int[] { 7, -7 },

            // Jobb lent átló
            new int[] { 1, 1 },
            new int[] { 2, 2 },
            new int[] { 3, 3 },
            new int[] { 4, 4 },
            new int[] { 5, 5 },
            new int[] { 6, 6 },
            new int[] { 7, 7 },

            // Bal lent átló
            new int[] { -1, 1 },
            new int[] { -2, 2 },
            new int[] { -3, 3 },
            new int[] { -4, 4 },
            new int[] { -5, 5 },
            new int[] { -6, 6 },
            new int[] { -7, 7 },

            // Bal fent átló
            new int[] { -1, -1 },
            new int[] { -2, -2 },
            new int[] { -3, -3 },
            new int[] { -4, -4 },
            new int[] { -5, -5 },
            new int[] { -6, -6 },
            new int[] { -7, -7 },
        };
        }
    }
}
