﻿// <copyright file="Queen.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel.Pieces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A queen piece.
    /// </summary>
    public class Queen : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Queen"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">the name of the piece.</param>
        public Queen(bool color, string name)
            : base(color, name)
        {
            this.Moves = new List<int[]>()
            {
                // Rook moves
                new int[] { 0, 1 },
                new int[] { 0, 2 },
                new int[] { 0, 3 },
                new int[] { 0, 4 },
                new int[] { 0, 5 },
                new int[] { 0, 6 },
                new int[] { 0, 7 },

                new int[] { 0, -1 },
                new int[] { 0, -2 },
                new int[] { 0, -3 },
                new int[] { 0, -4 },
                new int[] { 0, -5 },
                new int[] { 0, -6 },
                new int[] { 0, -7 },

                new int[] { 1, 0 },
                new int[] { 2, 0 },
                new int[] { 3, 0 },
                new int[] { 4, 0 },
                new int[] { 5, 0 },
                new int[] { 6, 0 },
                new int[] { 7, 0 },

                new int[] { -1, 0 },
                new int[] { -2, 0 },
                new int[] { -3, 0 },
                new int[] { -4, 0 },
                new int[] { -5, 0 },
                new int[] { -6, 0 },
                new int[] { -7, 0 },

                // Bishop moves
                new int[] { 1, 1 },
                new int[] { 2, 2 },
                new int[] { 3, 3 },
                new int[] { 4, 4 },
                new int[] { 5, 5 },
                new int[] { 6, 6 },
                new int[] { 7, 7 },

                new int[] { -1, 1 },
                new int[] { -2, 2 },
                new int[] { -3, 3 },
                new int[] { -4, 4 },
                new int[] { -5, 5 },
                new int[] { -6, 6 },
                new int[] { -7, 7 },

                new int[] { 1, -1 },
                new int[] { 2, -2 },
                new int[] { 3, -3 },
                new int[] { 4, -4 },
                new int[] { 5, -5 },
                new int[] { 6, -6 },
                new int[] { 7, -7 },

                new int[] { -1, -1 },
                new int[] { -2, -2 },
                new int[] { -3, -3 },
                new int[] { -4, -4 },
                new int[] { -5, -5 },
                new int[] { -6, -6 },
                new int[] { -7, -7 },
            };
        }
    }
}
