﻿// <copyright file="WhitePawn.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel.Pieces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A white pawn piece.
    /// </summary>
    public class WhitePawn : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WhitePawn"/> class.
        /// </summary>
        /// <param name="color">true - black or false - white.</param>
        /// <param name="name">the name of the piece.</param>
        public WhitePawn(bool color, string name)
            : base(color, name)
        {
            this.Moves = new List<int[]>()
            {
                new int[] { 0, -1 },
                new int[] { 0, -2 },
                new int[] { -1, -1 },
                new int[] { 1, -1 },
            };
        }
    }
}
