﻿// <copyright file="Tile.cs" company="RCN5HX">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Chess.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// One tile on the 8x8 chess board.
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tile"/> class.
        /// </summary>
        /// <param name="x">the x coordinate.</param>
        /// <param name="y">the y coordinate.</param>
        public Tile(int x, int y)
        {
            this.Dx = x;
            this.Dy = y;
        }

        /// <summary>
        /// Gets or Sets the x coordinate on the board.
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or Sets the y coordinate on the board.
        /// </summary>
        public int Dy { get; set; }

        /// <summary>
        /// Gets or Sets the piece on the current tile.
        /// </summary>
        public GameItem Piece { get; set; }
    }
}
