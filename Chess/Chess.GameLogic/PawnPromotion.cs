﻿// <copyright file="PawnPromotion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Chess.GameLogic
{
    using Chess.GameModel;

    /// <summary>
    /// When the pawn go to the enemy spawn
    /// in the last row it get promoted to chosen one.
    /// </summary>
    public class PawnPromotion
    {
        /// <summary>
        /// Model for change piece.
        /// </summary>
        private Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="PawnPromotion"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        public PawnPromotion(Model model)
        {
            this.model = model;
        }

        /// <summary>
        /// Promote the finish row pawn
        /// by the user chose.
        /// </summary>
        /// <param name="currentPos">Current piece finish position.</param>
        /// <param name="piece">New piece to replace the pawn.</param>
        public static void Promotion(Tile currentPos, GameItem piece)
        {
            currentPos.Piece = piece;
        }
    }
}
