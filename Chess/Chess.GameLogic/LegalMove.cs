﻿// <copyright file="LegalMove.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Chess.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Chess.GameLogic.MoveInterface;
    using Chess.GameModel;

    /// <summary>
    /// Which piece you can place can be placed.
    /// </summary>
    public class LegalMove : ILegalMove
    {
        private MovePieces movePiece;
        private GameItem store;
        private bool valid;

        /// <summary>
        /// Initializes a new instance of the <see cref="LegalMove"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        public LegalMove(IGameModel model)
        {
            this.Model = model;
        }

        /// <summary>
        /// Gets or sets model.
        /// </summary>
        private IGameModel Model { get; set; }

        /// <summary>
        /// Help the renderer to give back all the
        /// possible legal move by de basic rules.
        /// Move piece to test all possible outcome.
        /// </summary>
        /// <param name="currentPiece">Get the lifted piece position.</param>
        /// <returns>Bool matrix. True where the player can place it.</returns>
        public bool[,] SomeRenderWhenCheck(Tile currentPiece)
        {
            bool[,] whereToMove = new bool[this.Model.Board.Size, this.Model.Board.Size];
            this.movePiece = new MovePieces(this.Model);

            bool[,] currentPieaceLegalMove = this.IsLegal(this.Model.Board.Tiles[currentPiece.Dx, currentPiece.Dy]);

            for (int x = 0; x < this.Model.Board.Size; x++)
            {
                for (int y = 0; y < this.Model.Board.Size; y++)
                {
                    if (currentPieaceLegalMove[x, y] == true)
                    {
                        this.store = this.Model.Board.Tiles[x, y].Piece;
                        MovePieces.Move(this.Model.Board.Tiles[x, y], this.Model.Board.Tiles[currentPiece.Dx, currentPiece.Dy]);
                        this.valid = this.IsCheck(this.Model.Board.Tiles[x, y].Piece.Color);

                        // Ha a változtatások ellenére még mindig sakkban van.
                        if (this.valid)
                        {
                            MovePieces.Move(this.Model.Board.Tiles[currentPiece.Dx, currentPiece.Dy], this.Model.Board.Tiles[x, y]);
                            this.Model.Board.Tiles[x, y].Piece = this.store;
                        }
                        else
                        {
                            MovePieces.Move(this.Model.Board.Tiles[currentPiece.Dx, currentPiece.Dy], this.Model.Board.Tiles[x, y]);
                            this.Model.Board.Tiles[x, y].Piece = this.store;
                            whereToMove[x, y] = true;
                        }
                    }
                }
            }

            return whereToMove;
        }

        /// <summary>
        /// Matt methode.
        /// </summary>
        /// <param name="color">Get the color of the current player.</param>
        /// <returns>Basic true return value.</returns>
        public bool Matt(bool color)
        {
            bool matt = true;

            // Összes azonos színű bábút megnézzük
            for (int x = 0; x < this.Model.Board.Size; x++)
            {
                for (int y = 0; y < this.Model.Board.Size; y++)
                {
                    if (matt && this.Model.Board.Tiles[x, y].Piece != null && this.Model.Board.Tiles[x, y].Piece.Color == color)
                    {
                        // Megnézzük, hogy sakkba kerülés esetén tud e máshová lépni akármelyik bábúval
                        bool[,] resultOfPiece = this.SomeRenderWhenCheck(this.Model.Board.Tiles[x, y]);
                        for (int a = 0; a < this.Model.Board.Size; a++)
                        {
                            for (int b = 0; b < this.Model.Board.Size; b++)
                            {
                                // Ha a mátrix-nak van visszatérő igaz értéke, akkor
                                // az egy valid lépési lehetőség azaz még nem kapott mattot
                                if (resultOfPiece[a, b] == true)
                                {
                                    matt = false;
                                    return matt;
                                }
                            }
                        }
                    }
                }
            }

            return matt;
        }

        /// <summary>
        /// Where is it legal to move the designated piece.
        /// </summary>
        /// <param name="currentPos">Designated piece.</param>
        /// <returns>Bool matrix where true piece cen be placed.</returns>
        public bool[,] IsLegal(Tile currentPos)
        {
            List<int> firstBarrier = new List<int>();

            bool[,] legalMoves = new bool[this.Model.Board.Size, this.Model.Board.Size];

            for (int u = 0; u < currentPos?.Piece.Moves.Count; u++)
            {
                int alternativeX = currentPos.Dx + currentPos.Piece.Moves[u][0];
                int alternativeY = currentPos.Dy + currentPos.Piece.Moves[u][1];

                // Ha az új lépés a 8x8-as táblán kívülre esne akármelyik koordinátája, akkor meg se kell nézni azt, hogy a lépés engedélyezett lehetne e
                if (alternativeX <= 7 & alternativeX >= 0 & alternativeY <= 7 & alternativeY >= 0)
                {
                    // True --- fekete
                    // False --- feher

                    //---------------------------------------------------------------------------------   PAWN    ---------------------------------------------------------------------------------
                    if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "P")
                    {
                        if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece == null & (alternativeX == currentPos.Dx))
                        {
                            if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color & currentPos.Dy == 1 & this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy + 1].Piece == null
                                || this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color == false & currentPos.Dy == 6 & this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy - 1].Piece == null)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                            }
                            else if (alternativeY == currentPos.Dy + 1 | alternativeY == currentPos.Dy - 1)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                            }
                        }

                        // Ha bal átló lefelé megy a fekete és más színű a másik hely
                        else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null)
                        {
                            if (alternativeX < currentPos.Dx & this.Model.Board.Tiles[alternativeX, alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                            }

                            // Ugyan az jobb alsó átlóra
                            else if (alternativeX > currentPos.Dx & (this.Model.Board.Tiles[alternativeX, alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color))
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                            }
                        }
                    }

                    //---------------------------------------------------------------------------------   KNIGHT    ---------------------------------------------------------------------------------
                    else if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "N")
                    {
                        // Szabad az a hely, ahová lépni akar VAGY van ott egy enemy, ami nem a király
                        if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece == null || this.Model.Board.Tiles[alternativeX, alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                        {
                            legalMoves[alternativeX, alternativeY] = true;
                        }
                    }

                    //---------------------------------------------------------------------------------   KING    ---------------------------------------------------------------------------------
                    else if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "K")
                    {
                        if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece == null
                            || (this.Model.Board.Tiles[alternativeX, alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color))
                        {
                            legalMoves[alternativeX, alternativeY] = true;
                        }
                    }

                    //---------------------------------------------------------------------------------   ROOK    ---------------------------------------------------------------------------------
                    else if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "R")
                    {
                        // Fellfelé
                        if (currentPos.Dx == alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeY == firstBarrier.Max())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Jobbra
                        else if (currentPos.Dy == alternativeY && alternativeX > currentPos.Dx)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeX == firstBarrier.Min())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Lefelé
                        else if (currentPos.Dx == alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeY == firstBarrier.Min())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Balra
                        else if (currentPos.Dy == alternativeY && alternativeX < currentPos.Dx)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeX == firstBarrier.Max())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }
                    }

                    //---------------------------------------------------------------------------------   BISHOP    ---------------------------------------------------------------------------------
                    else if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "B")
                    {
                        // Jobb Fellfelé Átló
                        if (currentPos.Dx < alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7 || alternativeY == 0)
                            {
                                List<int> firstBarrier1 = firstBarrier;
                                firstBarrier1.Clear();
                            }
                        }

                        // Jobb Le Átló
                        if (currentPos.Dx < alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7 || alternativeY == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Bal Le Átló
                        if (currentPos.Dx > alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7 || alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7 || alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 7 || alternativeX == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Bal Fel Átló
                        if (currentPos.Dx > alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 0 || alternativeY == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }
                    }

                    //---------------------------------------------------------------------------------   QUEEN    ---------------------------------------------------------------------------------
                    else if (this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Name == "Q")
                    {
                        //-----------------------------------------ROOK--------------------------------------------------
                        // Fellfelé
                        if (currentPos.Dx == alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeY == firstBarrier.Max())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Jobbra
                        else if (currentPos.Dy == alternativeY && alternativeX > currentPos.Dx)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeX == firstBarrier.Min())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Lefelé
                        else if (currentPos.Dx == alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeY == firstBarrier.Min())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Balra
                        else if (currentPos.Dy == alternativeY && alternativeX < currentPos.Dx)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color && alternativeX == firstBarrier.Max())
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        //--------------------------------------------   BISHOP    ----------------------------------------------------
                        // Jobb Fellfelé Átló
                        if (currentPos.Dx < alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Max()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7 || alternativeY == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Jobb Le Átló
                        if (currentPos.Dx < alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[firstBarrier.Min(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 7 || alternativeY == 7)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 7 || alternativeY == 7)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Bal Le Átló
                        if (currentPos.Dx > alternativeX && alternativeY > currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7 || alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[alternativeX, firstBarrier.Min()].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeY == 7 || alternativeX == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeY == 7 || alternativeX == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }

                        // Bal Fel Átló
                        if (currentPos.Dx > alternativeX && alternativeY < currentPos.Dy)
                        {
                            if (this.Possible(currentPos.Dx, currentPos.Dy, alternativeX, alternativeY, firstBarrier).Count == 0)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null
                                && this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece == this.Model.Board.Tiles[alternativeX, alternativeY].Piece
                                && this.Model.Board.Tiles[firstBarrier.Max(), alternativeY].Piece.Color != this.Model.Board.Tiles[currentPos.Dx, currentPos.Dy].Piece.Color)
                            {
                                legalMoves[alternativeX, alternativeY] = true;
                                if (alternativeX == 0 || alternativeY == 0)
                                {
                                    firstBarrier.Clear();
                                }
                            }
                            else if (alternativeX == 0 || alternativeY == 0)
                            {
                                firstBarrier.Clear();
                            }
                        }
                    }
                }
            }

            return legalMoves;
        }

        /// <summary>
        /// Find the king. Where are u?.
        /// </summary>
        /// <param name="color">Get the color of the serached king.</param>
        /// <returns>With the king tile pos.</returns>
        public Tile WhereIsTheKing(bool color)
        {
            for (int a = 0; a < this.Model.Board.Size; a++)
            {
                for (int b = 0; b < this.Model.Board.Size; b++)
                {
                    if (this.Model.Board.Tiles[a, b].Piece != null
                        && this.Model.Board.Tiles[a, b].Piece.Color == color
                        && this.Model.Board.Tiles[a, b].Piece.Name == "K")
                    {
                        return this.Model.Board.Tiles[a, b];
                    }
                }
            }

            return this.Model.Board.Tiles[0, 0];
        }

        /// <summary>
        /// Go trough every piece every possible steps and if it is
        /// tue and ther is the opponent king it is a chess.
        /// </summary>
        /// <param name="color">Enemy color.</param>
        /// <returns>Not check by default.</returns>
        public bool IsCheck(bool color)
        {
            bool check = false;
            Tile king = this.WhereIsTheKing(color);

            // Bejárjuk a táblát a többi bábú lépéseinek vizsgálata miatt
            for (int x = 0; x < this.Model.Board.Size; x++)
            {
                // Bejárjuk a táblát a többi bábú lépéseinek vizsgálata miatt
                for (int y = 0; y < this.Model.Board.Size; y++)
                {
                    // Ha nem üres a mező és találtunk egy a megadottal ellentétes színű bábút
                    if (this.Model.Board.Tiles[x, y].Piece != null && this.Model.Board.Tiles[x, y].Piece.Color != color)
                    {
                        // Megnézzük mik a lehetséges lépései az elemnek
                        bool[,] pieaceLegalMove = this.IsLegal(this.Model.Board.Tiles[x, y]);

                        // Ha gyalog a megtalált elem
                        if (this.Model.Board.Tiles[x, y].Piece.Name == "P")
                        {
                            // Ha átlóban van a lehetséges lépés és a királynak a koordinátája a lehetséges lépések között van
                            if (x != king.Dx && pieaceLegalMove[king.Dx, king.Dy] == true)
                            {
                                check = true;
                            }
                        }

                        // Ha a királynő koordinátája a lehetséges lépéseknél igaz akkor látja az [x,y] bábú a királyt
                        else if (pieaceLegalMove[king.Dx, king.Dy] == true)
                        {
                            check = true;
                        }
                    }
                }
            }

            return check;
        }

        /// Can use Tile instead of current or alternative x, y coordinates.
        /// This methode helps the IsLegal method by checking each quater of the table.
        /// It splits to 8 different to be examined move.
        /// <param name="currentPosX">Tile cuccent position x parameter.</param>
        /// <param name="currentPosY">Tile cuccent position y parameter.</param>
        /// <param name="alternativeX">Tile new position x parameter.</param>
        /// <param name="alternativeY">Tile new position y parameter.</param>
        /// <param name="firstBarrier">First "wall" element".</param>
        /// <returns>With the barrier list.</returns>
        private List<int> Possible(int currentPosX, int currentPosY, int alternativeX, int alternativeY, List<int> firstBarrier)
        {
            // Felfelé vagy jobb fent átló
            if ((currentPosX == alternativeX | currentPosX < alternativeX) & alternativeY < currentPosY && this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null)
            {
                firstBarrier.Add(alternativeY);
            }

            // Jobb vagy jobb le átló
            else if ((currentPosY < alternativeY | currentPosY == alternativeY) & currentPosX < alternativeX && this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null)
            {
                firstBarrier.Add(alternativeX);
            }

            // Lefelfelé vagy bal le átló
            else if ((currentPosX > alternativeX | currentPosX == alternativeX) & alternativeY > currentPosY && this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null)
            {
                firstBarrier.Add(alternativeY);
            }

            // Balra vagy bal fent átló
            else if ((currentPosY > alternativeY | currentPosY == alternativeY) & currentPosX > alternativeX && this.Model.Board.Tiles[alternativeX, alternativeY].Piece != null)
            {
                firstBarrier.Add(alternativeX);
            }

            return firstBarrier;
        }
    }
}
