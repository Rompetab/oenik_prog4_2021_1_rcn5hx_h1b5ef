﻿// <copyright file="ScoreCount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Chess.GameLogic
{
    using System.Collections.Generic;
    using Chess.GameModel;

    /// <summary>
    /// Count the achived score for each player.
    /// </summary>
    public class ScoreCount
    {
        private readonly Dictionary<string, int> scores = new Dictionary<string, int>() { { "P", 1 }, { "N", 3 }, { "B", 3 }, { "R", 5 }, { "Q", 9 }, { "K", 0 } };

        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreCount"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        public ScoreCount(IGameModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// Score of piece.
        /// </summary>
        /// <param name="piece">Piece.</param>
        /// <returns>Int piece value.</returns>
        public int Count(GameItem piece)
        {
            return this.scores[piece?.Name];
        }
    }
}
