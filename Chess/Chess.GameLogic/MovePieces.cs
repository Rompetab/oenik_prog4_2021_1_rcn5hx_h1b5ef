﻿// <copyright file="MovePieces.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Chess.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Chess.GameModel;

    /// <summary>
    /// Move one piece to other tile class and methodes and get tile position from mouse.
    /// </summary>
    public class MovePieces
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovePieces"/> class.
        /// </summary>
        /// <param name="model">Model.</param>
        public MovePieces(IGameModel model)
        {
            this.Model = model;
        }

        /// <summary>
        /// Gets or sets interface.
        /// </summary>
        private IGameModel Model { get; set; }

        /// <summary>
        /// Move to new location from old location.
        /// </summary>
        /// <param name="newTile">New tiles coordinates.</param>
        /// <param name="currentTile">Old tiles coordinates.</param>
        public static void Move(Tile newTile, Tile currentTile)
        {
            GameItem movePiece = currentTile?.Piece;
            currentTile.Piece = null;
            newTile.Piece = movePiece;
        }

        /// <summary>
        /// Pixel position.
        /// </summary>
        /// <param name="mousePos">Mouse position.</param>
        /// <returns>Mouse position pointer.</returns>
        public Point GetTilePos(Point mousePos) // Pixel position => Tile position
        {
            return new Point((int)(mousePos.X / this.Model.TileSize), (int)(mousePos.Y / this.Model.TileSize));
        }
    }
}
