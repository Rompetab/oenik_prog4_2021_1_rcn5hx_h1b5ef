﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "It is fix thet not null. The player need to chose new piece or can't get on the enemy last row tile.", Scope = "member", Target = "~M:Chess.GameLogic.PawnPromotion.Promotion(Chess.GameModel.Tile,Chess.GameModel.GameItem)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Can not be null again.", Scope = "member", Target = "~M:Chess.GameLogic.MovePieces.Move(Chess.GameModel.Tile,Chess.GameModel.Tile)")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged Array warning again.", Scope = "member", Target = "~M:Chess.GameLogic.MoveInterface.ILegalMove.SomeRenderWhenCheck(Chess.GameModel.Tile)~System.Boolean[,]")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged Array warning again.", Scope = "member", Target = "~M:Chess.GameLogic.MoveInterface.ILegalMove.IsLegal(Chess.GameModel.Tile)~System.Boolean[,]")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Can not get null parameter.", Scope = "member", Target = "~M:Chess.GameLogic.LegalMove.SomeRenderWhenCheck(Chess.GameModel.Tile)~System.Boolean[,]")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged array is not possible. Can not get cleaner or get better performance.", Scope = "member", Target = "~M:Chess.GameLogic.LegalMove.IsLegal(Chess.GameModel.Tile)~System.Boolean[,]")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Jagged array is not possible. Can not get cleaner or get better performance.", Scope = "member", Target = "~M:Chess.GameLogic.LegalMove.SomeRenderWhenCheck(Chess.GameModel.Tile)~System.Boolean[,]")]
