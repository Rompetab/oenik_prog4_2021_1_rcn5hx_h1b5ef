﻿// <copyright file="ILegalMove.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Chess.GameLogic.MoveInterface
{
    using System.Collections.Generic;
    using Chess.GameModel;

    /// <summary>
    /// Interface for the legal move class.
    /// </summary>
    public interface ILegalMove
    {
        /// <summary>
        /// It gives back a bool matrix which contains where can the player place
        /// the selected piece on the board and helps the renderer by the return type.
        /// </summary>
        /// <param name="currentPiece">"Touched" / selected piece.</param>
        /// <returns>Bool[,] possible place moves.</returns>
        public bool[,] SomeRenderWhenCheck(Tile currentPiece);

        /// <summary>
        /// The algoritm examines thet the given color player get a Matt.
        /// If it is true then the game ends and the other piece color wins.
        /// </summary>
        /// <param name="color">The color of the player to be examined.</param>
        /// <returns>Matt or the play can be continued.</returns>
        public bool Matt(bool color);

        /// <summary>
        /// Seelcted piece possible move outcome by the piece basic move rules
        /// and by the first barrier (wall) rule.
        /// </summary>
        /// <param name="currentPos">Selected piece.</param>
        /// <returns>A matrix where the true marks the valid move.</returns>
        public bool[,] IsLegal(Tile currentPos);

        /// <summary>
        /// Search the table for the given color king.
        /// This method helps to other methor for example IsCheck method.
        /// </summary>
        /// <param name="color">Color you want to look for.</param>
        /// <returns>With the position of the king.</returns>
        public Tile WhereIsTheKing(bool color);

        /// <summary>
        /// Helper method for the next ones:
        /// Need to check before every step to not enter a position that gives chess to current color.
        /// And watch if the next player got in chess by the previous step.
        /// </summary>
        /// <param name="color">Color of the current or coming player.</param>
        /// <returns>Default type is false and if it checkk it returns true.</returns>
        public bool IsCheck(bool color);
    }
}
